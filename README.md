# Possum Field Study Data Analysis in Python

This Python project is dedicated to the exploration of a real dataset obtained from a field study conducted on possums in two different locations: Victoria or New South Wales and Queensland. We will apply data mining techniques and various data analysis algorithms to gain insights into these fascinating creatures and address several key questions posed by the dataset providers.

## Project Overview

### Research Questions

We aim to answer the following questions using the possum dataset:

1. Can we use the total length to predict a possum's head length?
2. Which possum body dimensions are most correlated with age and sex?
3. Can we classify a possum's sex by its body dimensions and location?
4. Can we predict a possum's trapping location from its body dimensions?

### Dataset Description

The dataset is comprised of the following variables:

#### Categorical Variables:
- `case`: Identifier number of each possum.
- `site`: The site number where the possum was trapped.
- `Pop`: Population, categorized as either "Vic" (Victoria) or "other" (New South Wales or Queensland).
- `sex`: Sex of the possum, indicated as "m" (male) or "f" (female).

#### Continuous Variables (Body Dimensions):
- `age`: Age of the captured possum.
- `hdlngth`: Head length in millimeters.
- `skullw`: Skull width in millimeters.
- `totlngth`: Total length in centimeters.
- `taill`: Tail length in centimeters.
- `footlgth`: Foot length in millimeters.
- `earconch`: Ear conch length in millimeters.
- `eye`: Distance from medial canthus to lateral canthus of the right eye in millimeters.
- `chest`: Chest girth in centimeters.
- `belly`: Belly girth in centimeters.

## Project Goals

The primary objectives for this project are as follows:

- Exploratory Data Analysis (EDA) to better understand the dataset.
- Building predictive models to answer the research questions.
- Assessing the performance of different data analysis algorithms.
- Visualizing the results and findings.